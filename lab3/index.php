<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE-edge" />
    <meta
            name="viewport"
            content="width-device-width,initial-scale=1,maximum-scale=1"
    />

    <title>Students</title>
    <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0"
    />
    <link rel="stylesheet" href="styles.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!--<link rel="manifest" href="manifest.json" /> -->
</head>
<body>
<?php include("config.php"); ?>
<div class="container">
    <aside>
        <div class="top">
            <div class="logo">
                <h2>CMS</h2>
            </div>
            <div class="close" id="close-btn">
                <span class="material-symbols-outlined">close</span>
            </div>
        </div>

        <div class="sidebar">
            <a href="dashboard.html">
                <span class="material-symbols-outlined">dashboard</span>
                <h3>Dashboard</h3>
            </a>
            <a href="#" class="active">
                <span class="material-symbols-outlined">diversity_3</span>
                <h3>Students</h3>
            </a>
            <a href="tasks.html">
                <span class="material-symbols-outlined">task</span>
                <h3>Tasks</h3>
            </a>
        </div>
    </aside>

    <main>
        <div class="container1">
            <h1>
                <button id="menu-btn">
                    <span class="material-symbols-outlined">menu</span>
                </button>

                Students
            </h1>

            <div class="profile">
                <div class="notifications">
              <span class="material-symbols-outlined" id="messages"
              >notifications</span
              >
                    <div class="new-message"></div>
                    <div class="notifications-window">
                        <div class="message">
                            <div class="sender-photo"><img src="3135715.png" /></div>
                            <div>
                                <h4>Olia</h4>
                                <small>Hi everyone</small>
                            </div>
                        </div>
                        <div class="message">
                            <div class="sender-photo"><img src="3135715.png" /></div>
                            <div>
                                <h4>Olia</h4>
                                <small>Hi everyone</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-account">
                    <div class="profile-photo">
                        <img src="3135715.png" />
                        <div class="account">
                            <h3>Profile</h3>
                            <h3>Log out</h3>
                        </div>
                    </div>
                    <b>James Bond</b>
                </div>
            </div>
        </div>

        <div class="students">
            <div class="table-header">
                <button id="add-student" data-id="">
                    <span class="material-symbols-outlined">add</span>
                </button>
            </div>
            <div class="students-table">
                <table>
                    <thead>
                    <tr>
                        <th><input type="checkbox" /></th>
                        <th>Group</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Birthday</th>
                        <th>Status</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </main>

    <dialog class="dialogOverlay">
        <div class="form-container">
            <div class="form-close">
                <span class="material-symbols-outlined">close</span>
            </div>
            <div class="form add-form">
                <form>
                    <h2 class="dialog-heading">Add student</h2>
                    <input type="hidden" id="">
                    <div class="form-row">
                        <h3>Group</h3>
                        <div class="input-box">
                            <select id="group" name="Group" placeholder="Select group">
                                <option value="" disabled selected hidden>Select group</option>
                                <?php foreach($arrGroup as $key => $group){ ?>
                                <option value="<?= $key ?>"><?= $group ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    </div>
                    <p class="dialog-warning" id="dialog-group-warning">Please, enter this field</p>
                    <div class="form-row">
                        <h3>First name</h3>
                        <div class="input-box">
                            <input id="name" type="first-name" />
                        </div>

                    </div>
                    <p class="dialog-warning" id="dialog-name-warning">Please, enter this field</p>
                    <div class="form-row">
                        <h3>Last name</h3>
                        <div class="input-box">
                            <input id="surname" type="last-name" />
                        </div>

                    </div>
                    <p class="dialog-warning" id="dialog-surname-warning">Please, enter this field</p>
                    <div class="form-row">
                        <h3>Gender</h3>
                        <div class="input-box">
                            <select id="gender" name="Gender" placeholder="Select gender">
                                <option value="" disabled selected hidden>
                                    Select gender
                                </option>
                                <?php foreach($arrGender as $key => $gender){ ?>
                                    <option value="<?= $key ?>"><?= $gender ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    </div>
                    <p class="dialog-warning" id="dialog-gender-warning">Please, enter this field</p>
                    <div class="form-row">
                        <h3>Birthday</h3>
                        <div class="input-box"><input id="birthday" type="date" /></div>
                    </div>
                    <p class="dialog-warning" id="dialog-birthday-warning">Please, enter this field</p>
                    <div class="form-row">
                        <h3>Status</h3>
                        <div class="checkbox-wrapper"><input id="status" type="checkbox" /></div>
                    </div>
                    <div class="dialog-buttons-container">
                        <button class="dialog-cancel">Cancel</button>
                        <button class="dialog-submit">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </dialog>

    <dialog class="delete-student-dialog">
        <div class="delete-student-container">
            <div class="delete-student-close">
                <span class="material-symbols-outlined">close</span>
            </div>
            <div class="delete-student">
                <h2 class="dialog-heading">Delete student</h2>
                <p>Are you sure you want to delete this student?</p>
                <div class="delete-student-buttons">
                    <button class="delete-student-cancel">Cancel</button>
                    <button class="delete-student-confirm">Delete</button>
                </div>
            </div>
        </div>
    </dialog>
</div>
<script src="index.js"></script>
</body>
</html>