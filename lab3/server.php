<?php
header('Content-Type: application/json');

include 'config.php';

$response = array(
    'success' => true,
    'error' => array(
        'group' => true,
        'firstName' => true,
        'lastName' => true,
        'gender' => true,
        'birthday' => true,
        'message' => ""
    )
);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['id']) && isset($_POST['group']) && isset($_POST['firstName']) && isset($_POST['lastName']) && isset($_POST['gender']) && isset($_POST['birthday'])) {
        $group = $_POST['group'];
        $firstName = $_POST['firstName'];
        $lastName = $_POST['lastName'];
        $gender = $_POST['gender'];
        $birthday = $_POST['birthday'];

        if(!isset($arrGroup[$group])) {
            $response['success'] = false;
            $response['error']['group'] = false;
            $response['error']['message'] .= " Група не вказана";
        }
        if($firstName == "") {
            $response['success'] = false;
            $response['error']['firstName'] = false;
            $response['error']['message'] .= " Ім'я не вказане";
        }
        if($lastName == "") {
            $response['success'] = false;
            $response['error']['lastName'] = false;
            $response['error']['message'] .= " Прізвище не вказане";
        }
        if(!isset($arrGender[$gender])) {
            $response['success'] = false;
            $response['error']['gender'] = false;
            $response['error']['message'] .= " Гендер не вказаний";
        }
        if(!DateTime::createFromFormat('d.m.Y', $birthday)) {
            $response['success'] = false;
            $response['error']['birthday'] = false;
            $response['error']['message'] .= " Дата народження не вказана";
        }
    }
    else {
        $response['success'] = false;
        $response['error']['message'] .= " У POST-запиті не вистачає полів";
    }
} else {
    $response['success'] = false;
    $response['error']['message'] .= " Це не POST-запит";
}

echo json_encode($response);
?>