const connections = [];
const io = require('socket.io')(3000, {
    cors: {
        origin: "http://localhost",
    },
});
const { MongoClient } = require('mongodb');

const uri = 'mongodb+srv://gnatetskaolia:GxptPiV3f21hbMRU@cluster0.yfxgety.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0';
const client = new MongoClient(uri);

io.on('connection', async (socket) => {
    connections.push(socket);

    socket.on('disconnect', async () => {
        connections.splice(connections.indexOf(socket), 1);
        io.sockets.emit('get user', await getLoggedInUsers());
    });

    socket.on('send message', async (data, info) => {
        if (info === "all") {
            io.sockets.emit('new message', { msg: data, user: socket.username, chat: info, socketId: socket.id });
        } else {
            const recipientSocket = connections.find(connection => connection.username === info);
            if (recipientSocket) {
                recipientSocket.emit('new message', { msg: data, user: socket.username, chat: socket.username, socketId: socket.id });
            }
            socket.emit('new message', { msg: data, user: socket.username, chat: info, socketId: socket.id });
        }

        try {
            const database = await client.connect();
            const collection = database.db('chat').collection('messages');

            const messageData = {
                message: data,
                sender: socket.username,
                chat: info,
                socketId: socket.id,
            };

            const result = await collection.insertOne(messageData);
            console.log(`Data successfully written with _id: ${result.insertedId}`);
        } catch (error) {
            console.error("An error occurred when writing a message to the database:", error);
        }
    });

    socket.on("new user", async (data) => {
        socket.username = data;

        await addUserToDatabase(data, socket.id);
        io.sockets.emit('get user', await getLoggedInUsers());

        try {
            const database = await client.connect();
            const collection = database.db('chat').collection('messages');

            const messages = await collection.find({ $or: [{ sender: data }, { chat: data }, { chat: "all" }] }).toArray();
            console.log(messages);
            socket.emit('get chats', { chat: messages });
        } catch (error) {
            console.error("Error reading messages from the database:", error);
        }
    });
});

async function addUserToDatabase(username, socketId) { // Приймаємо також ідентифікатор сокета
    try {
        const database = await client.connect();
        const collection = database.db('chat').collection('users');

        const existingUser = await collection.findOne({ username: username });

        if (!existingUser) {
            await collection.insertOne({ username: username, socketId: socketId }); // Зберігаємо ідентифікатор сокета в базі
        }
    } catch (error) {
        console.error("Error adding new user to database:", error);
    }
}

async function getLoggedInUsers() {
    try {
        const database = await client.connect();
        const collection = database.db('chat').collection('users');

        const users = await collection.find({}).toArray();
        const usernames = users.map(user => user.username);
        return usernames;
    } catch (error) {
        console.error("Error retrieving the list of users from the database:", error);
        return [];
    }
}