<?php
header('Content-Type: application/json');

include 'config.php';

$response = [
    'success' => true,
    'error' => ['code' => null, 'message' => null],
    'id' => ""
];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['id']) && isset($_POST['group']) && isset($_POST['firstName']) && isset($_POST['lastName']) && isset($_POST['gender']) && isset($_POST['birthday']) && isset($_POST['status'])) {
        $id = $_POST['id'];
        $group = $_POST['group'];
        $firstName = $_POST['firstName'];
        $lastName = $_POST['lastName'];
        $gender = $_POST['gender'];
        $birthday = $_POST['birthday'];
        $status = $_POST['status'];

        if(!isset($arrGroup[$group])) {
            $response['success'] = false;
            $response['error']['code'] = 1;
            $response['error']['message'] = "Incorrect group";
        }
        if($firstName == "") {
            $response['success'] = false;
            $response['error']['code'] = 2;
            $response['error']['message'] = "Incorrect name";
        }
        if($lastName == "") {
            $response['success'] = false;
            $response['error']['code'] = 3;
            $response['error']['message'] = "Incorrect surname";
        }
        if(!isset($arrGender[$gender])) {
            $response['success'] = false;
            $response['error']['code'] = 4;
            $response['error']['message'] = "Incorrect gender";
        }
        if(!strtotime($birthday)) {
            $response['success'] = false;
            $response['error']['code'] = 5;
            $response['error']['message'] = "Incorrect date of birth";
        }

        if ($response['success'] == true) {
            if ($id !== "") {
                $response['id'] = $id;
                $sql = "UPDATE students SET `group` = '$group', firstName = '$firstName', lastName = '$lastName', gender = '$gender', birthday = '$birthday', status = '$status'  WHERE id = '$id'";
            }
            else {
                $sql = "INSERT INTO students (`group`, firstName, lastName, gender, birthday, status) VALUES ('$group', '$firstName', '$lastName', '$gender', '$birthday', '$status')";
            }

            if ($conn->query($sql) === TRUE) {
                $response['id'] = mysqli_insert_id($conn);
            } else {
                $response['success'] = false;
                $response['error']['message'] = "Error inserting data to the database";
            }
        }
    }
    else {
        $response['success'] = false;
        $response['error']['message'] = "There are not enough fields in the POST-request";
    }
} else {
    $response['success'] = false;
    $response['error']['message'] = "This isn`t POST-request";
}

echo json_encode($response);
?>