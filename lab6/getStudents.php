<?php
include "config.php";


$sql = "SELECT * FROM students";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $students = $result->fetch_all(MYSQLI_ASSOC);
    foreach ($students as $student) {
        echo "<tr data-id='" . $student['id'] . "'>";
        echo "<td><input type='checkbox'></td>";
        echo "<td data-val='" . $student['group'] . "'>" . $arrGroup[$student['group']] . "</td>";
        echo "<td>" . $student['firstName'] . " " . $student['lastName'] . "</td>";
        echo "<td data-val='" . $student['gender'] . "'>" . $arrGender[$student['gender']] . "</td>";
        echo "<td>" . $student['birthday'] . "</td>";
        echo "<td>" . ($student['status'] ? '<span class="status active"></span>' : '<span class="status nonactive"></span>') . "</td>";
        echo "<td>";
        echo "<button class='table-buttons edit-btn' data-id='" . $student['id'] . "'><span class='material-symbols-outlined'>edit</span></button>";
        echo "<button class='table-buttons delete-btn' data-id='" . $student['id'] . "'><span class='material-symbols-outlined'>delete</span></button>";
        echo "</td>";
        echo "</tr>";
    }
}