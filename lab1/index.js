const sideMenu = document.querySelector("aside");
const menuBtn = document.querySelector("#menu-btn");
const closeBtn = document.querySelector("#close-btn");
const addStudentBtn = document.querySelector('#add-student');
const deleteStudentButtons = document.querySelectorAll('.delete-btn');

menuBtn.addEventListener('click', () => {
    sideMenu.style.display = 'block';
});

closeBtn.addEventListener('click', () => {
    sideMenu.style.display = 'none';
});

addStudentBtn.addEventListener('click', () => {
    // Отримайте посилання на тіло таблиці
    let tableBody = document.querySelector('tbody');

    // Створіть новий рядок
    let newRow = document.createElement('tr');

    // Створіть першу комірку з чекбоксом
    let cell1 = document.createElement('td');
    let checkbox = document.createElement('input');
    checkbox.setAttribute('type', 'checkbox');
    checkbox.setAttribute('id', 'myCheckbox2');
    let label = document.createElement('label');
    label.setAttribute('for', 'myCheckbox2');
    cell1.appendChild(checkbox);
    cell1.appendChild(label);

    // Створіть комірки з текстовими даними
    let cell2 = document.createElement('td');
    cell2.textContent = 'PZ-21';
    let cell3 = document.createElement('td');
    cell3.textContent = 'Olha Hnatetska';
    let cell4 = document.createElement('td');
    cell4.textContent = 'Female';
    let cell5 = document.createElement('td');
    cell5.textContent = '09.02.2005';
    let cell6 = document.createElement('td');
    let spanStatus = document.createElement('span');
    spanStatus.setAttribute('class', 'status active');
    cell6.appendChild(spanStatus);
    let cell7 = document.createElement('td');
    let editButton = document.createElement('button');
    editButton.setAttribute('class', 'table-buttons');
    editButton.innerHTML = '<span class="material-symbols-outlined">edit</span>';
    let deleteButton = document.createElement('button');
    deleteButton.setAttribute('class', 'table-buttons');
    deleteButton.innerHTML = '<span class="material-symbols-outlined">delete</span>';
    deleteButton.addEventListener('click', () => deleteStudents(deleteButton));
    cell7.appendChild(editButton);
    cell7.appendChild(deleteButton);

    // Додайте всі комірки до нового рядка
    newRow.appendChild(cell1);
    newRow.appendChild(cell2);
    newRow.appendChild(cell3);
    newRow.appendChild(cell4);
    newRow.appendChild(cell5);
    newRow.appendChild(cell6);
    newRow.appendChild(cell7);

    // Додайте новий рядок в тіло таблиці
    tableBody.appendChild(newRow);
});

deleteStudentButtons.forEach((button) => {button.addEventListener('click', () => deleteStudents(button))});

function deleteStudents(button) {
    let row = button.closest('tr');

    // Видалити рядок <tr>
    if (row) {
        row.remove();
    } else {
        console.log("Рядок не знайдено");
    }
}
