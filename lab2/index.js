window.onload = async function () {
    if (navigator.serviceWorker) {
        try {
            await navigator.serviceWorker.register("serviceWorker.js");
        } catch (error) {
            console.log("Can't register serviceWorker", error);
        }
    }
};

const sideMenu = document.querySelector("aside");
const menuBtn = document.querySelector("#menu-btn");
const closeBtn = document.querySelector("#close-btn");
const deleteStudentButtons = document.querySelectorAll('.delete-btn');
const addEditStudentButtons = document.querySelectorAll('.add-edit-btn');
const cancelDeleteDialogButton = document.querySelector(
    ".delete-student-cancel"
);
const closeDeleteDialogButton = document.querySelector(".delete-student-close");
const submitDialogButton = document.querySelector(".dialog-submit");
const closeAddEditDialogButton = document.querySelector(".form-close");
const cancelButton = document.querySelector(".dialog-cancel");

//document.addEventListener('DOMContentLoaded', function() {
//    let rows = document.querySelectorAll('tr');
//    for (i=0; i<rows.length; ++i) {
//        rows[i].setAttribute('data-id', Math.random().toString(36).substring(2, 15));
//    }
//});

menuBtn.addEventListener('click', () => {
    sideMenu.style.display = 'block';
});

closeBtn.addEventListener('click', () => {
    sideMenu.style.display = 'none';
});

deleteStudentButtons.forEach((button) => {button.addEventListener('click', () => deleteDialogFunction(button))});
addEditStudentButtons.forEach((button) => {button.addEventListener('click', () => AddAndEditDialogFunction(button))});

closeAddEditDialogButton.addEventListener("click", () => {
    let dialog = document.querySelector(".dialogOverlay");
    dialog.close();
})

cancelDeleteDialogButton.addEventListener("click", () => {
    let deleteDialog = document.querySelector(".delete-student-dialog");
    deleteDialog.close();
});

closeDeleteDialogButton.addEventListener("click", () => {
    let deleteDialog = document.querySelector(".delete-student-dialog");
    deleteDialog.close();
});

cancelButton.addEventListener("click", () => {
    let dialog = document.querySelector(".dialogOverlay");
    dialog.close();
})

function deleteDialogFunction(button) {
    let deleteDialog = document.querySelector(".delete-student-dialog");
    deleteDialog.showModal();

    let deleteButton = document.querySelector(".delete-student-confirm");
    deleteButton.addEventListener("click", function () {
        deleteStudents(button);
        deleteDialog.close();
    });
}

function deleteStudents(button) {
    let row = button.closest('tr');

    if (row) {
        row.remove();
    } else {
        console.log("Рядок не знайдено");
    }
}

//Додавання та редагування студента
function AddAndEditDialogFunction(button) {
    console.log(button);
    let buttonId = button.getAttribute('data-id');

    if (buttonId) {
        console.log("Edit button");
        console.log(buttonId);
        EditDialogFunction(buttonId);
    }
    else {
        console.log("Add button");
        AddDialogFunction();
    }
}

function EditDialogFunction(id) {
    clearForm();

    console.log(id);
    let rows = document.querySelectorAll('tr');

    for (i=0; i<rows.length; ++i) {
        console.log(rows[i].getAttribute('data-id'));
        if (rows[i].getAttribute('data-id') == id) {
            row = rows[i];
            break;
        }
    }

    let dialogOverlay = document.querySelector(".dialogOverlay");
    let form = document.querySelector("form");
    let formID = form.querySelector("input[type=hidden]");
    formID.id = row.getAttribute('data-id');
    console.log(formID.id + " FORM ID");

    dialogOverlay.showModal();

    let formTitle = document.querySelector(".dialog-heading");
    formTitle.innerHTML = "Edit student";

    const group = document.querySelector("#group");
    const name = document.querySelector("#name");
    const surname = document.querySelector("#surname");
    const gender = document.querySelector("#gender");
    const birthday = document.querySelector("#birthday");
    const status = document.querySelector("#status");

    console.log(row.cells[1].innerHTML);

    for (let i = 1; i < group.options.length; ++i) {
        if (row.cells[1].innerText == group.querySelector('option[value="' + i + '"]').textContent) {
            console.log("i=" + i);
            group.value = i;
            break;
        }
    }

    for (let i = 1; i < gender.options.length; ++i) {
        if (row.cells[3].innerText == gender.querySelector('option[value="' + i + '"]').textContent) {
            console.log("i=" + i);
            gender.value = i;
            break;
        }
    }

    let nameArray = row.cells[2].innerHTML.split(" ");
    name.value = row.querySelector('[data-id="firstname"]').textContent;
    surname.value = row.querySelector('[data-id="lastname"]').textContent;

    let birthdayArray = row.cells[4].innerHTML.split(".");
    birthday.value = `${birthdayArray[2]}-${birthdayArray[1]}-${birthdayArray[0]}`;

    let statusRow = row.cells[5].querySelector("span");

    status.checked = statusRow.classList.contains("active")? true : false;
}

function AddDialogFunction() {
    let form = document.querySelector(".dialogOverlay");
    clearForm();

    let formTitle = document.querySelector(".dialog-heading");
    formTitle.innerHTML = "Add student";

    form.showModal();
}

submitDialogButton.addEventListener("click", (event) => {
    event.preventDefault();
    if (ValidateDialog()) {
        let formID = document.querySelector("input[type=hidden]").id;
        console.log(formID + "id");

        if (formID) {
            EditStudent(formID);
        } else {
            AddStudent();
        }

        let form = document.querySelector(".dialogOverlay");
        form.close();
    }
});

function EditStudent(formID) {
    const form = document.querySelector(".dialogOverlay");

    let row = null;
    let rows = document.querySelectorAll('tr');
    for (i = 0; i < rows.length; ++i) {
        if (rows[i].getAttribute('data-id') == formID) {
            row = rows[i];
        }
    }

    const group = document.querySelector("#group");
    const name = document.querySelector("#name");
    const surname = document.querySelector("#surname");
    const gender = document.querySelector("#gender");
    const birthday = document.querySelector("#birthday");
    const status = document.querySelector("#status")

    let formattedBirthday = "";
    let birthdayArray = birthday.value.split("-");
    formattedBirthday = `${birthdayArray[2]}.${birthdayArray[1]}.${birthdayArray[0]}`;

    row.cells[1].innerHTML = group.querySelector('option[value="' + group.value + '"]').textContent;
    row.cells[2].innerHTML = `<span data-id="firstname">${name.value}</span> <span data-id="lastname">${surname.value}</span>`;
    row.cells[3].innerHTML = gender.querySelector('option[value="' + gender.value + '"]').textContent;
    row.cells[4].innerHTML = formattedBirthday;
    row.cells[5].innerHTML = status.checked ? `<span class="status active"></span>` : `<span class="status nonactive"></span>`;

    const formTitle = document.querySelector(".dialog-heading");
    formTitle.innerHTML = "Add student";

    clearForm();

    form.close();
}

function AddStudent() {
    const group = document.querySelector("#group");
    const name = document.querySelector("#name");
    const surname = document.querySelector("#surname");
    const gender = document.querySelector("#gender");
    const birthday = document.querySelector("#birthday");
    const status = document.querySelector("#status");

    let formattedBirthday = "";
    let birthdayArray = birthday.value.split("-");
    formattedBirthday = `${birthdayArray[2]}.${birthdayArray[1]}.${birthdayArray[0]}`;

    const table = document.querySelector("table");
    const tableBody = table.querySelector("tbody");

    // console.log(group.value);
    let rows = tableBody.querySelectorAll('tr');
    let rowId = Math.random().toString(36).substring(2, 15);
    console.log(rowId);

    for (i=0; i<rows.length; ++i) {
        if(rowId==rows[i].getAttribute('data-id'))
            rowId = Math.random().toString(36).substring(2, 15);
    }

    // let newRow = document.createElement("tr");
    let newRow=tableBody.insertRow();
    newRow.insertCell(0).innerHTML = `<input type="checkbox">`;
    newRow.insertCell(1).innerHTML = group.querySelector('option[value="' + group.value + '"]').textContent;
    newRow.insertCell(2).innerHTML = `<span data-id="firstname">${name.value}</span> <span data-id="lastname">${surname.value}</span>`;
    newRow.insertCell(3).innerHTML = gender.querySelector('option[value="' + gender.value + '"]').textContent;
    newRow.insertCell(4).innerHTML = formattedBirthday;
    newRow.insertCell(5).innerHTML = status.checked ? '<span class="status active"></span>' : '<span class="status nonactive"></span>';
    newRow.insertCell(
        6
    ).innerHTML = `<button class="table-buttons" data-id=${rowId}><span class="material-symbols-outlined add-edit-btn">edit</span></button><button class="table-buttons delete-btn" data-id=${rowId}><span class="material-symbols-outlined">delete</span></button>`;

    newRow.setAttribute('data-id', rowId);
    console.log(newRow.getAttribute('data-id'));

    let rowData = {
        id: rowId,
        group: group.value,
        fullName: name.value + ' ' + surname.value,
        gender: gender.value,
        birthday: formattedBirthday,
        status: status.checked ? true : false
    };

    let jsonRow = JSON.stringify(rowData);
    console.log(jsonRow);

    tableBody.appendChild(newRow);

    newRow
        .querySelector(".delete-btn")
        .addEventListener("click", () =>
            deleteDialogFunction(newRow.querySelector(".delete-btn"))
        );
    newRow.querySelector(".delete-btn").setAttribute('data-id', rowId);
    newRow.querySelector(".add-edit-btn").addEventListener("click", () => AddAndEditDialogFunction(newRow.querySelector(".add-edit-btn")));
    newRow.querySelector(".add-edit-btn").setAttribute('data-id', rowId);
}

function ValidateDialog() {
    const group = document.querySelector("#group");
    const name = document.querySelector("#name");
    const surname = document.querySelector("#surname");
    const gender = document.querySelector("#gender");
    const birthday = document.querySelector("#birthday");

    const groupWarning = document.querySelector("#dialog-group-warning");
    const nameWarning = document.querySelector("#dialog-name-warning");
    const surnameWarning = document.querySelector("#dialog-surname-warning");
    const genderWarning = document.querySelector("#dialog-gender-warning");
    const birthdayWarning = document.querySelector("#dialog-birthday-warning");

    let isValid = true;

    if (group.value === "") {
        groupWarning.style.display = "block";
        isValid = false;
    } else {
        groupWarning.style.display = "none";
    }

    if (name.value === "") {
        nameWarning.style.display = "block";
        isValid = false;
    } else {
        nameWarning.style.display = "none";
    }

    if (surname.value === "") {
        surnameWarning.style.display = "block";
        isValid = false;
    } else {
        surnameWarning.style.display = "none";
    }

    if (gender.value === "") {
        genderWarning.style.display = "block";
        isValid = false;
    } else {
        genderWarning.style.display = "none";
    }

    if (birthday.value === "") {
        birthdayWarning.style.display = "block";
        isValid = false;
    } else {
        birthdayWarning.style.display = "none";
    }

    return isValid;
}

function clearForm() {
    const formId = document.querySelector("input[type=hidden]");
    const group = document.querySelector("#group");
    const name = document.querySelector("#name");
    const surname = document.querySelector("#surname");
    const gender = document.querySelector("#gender");
    const birthday = document.querySelector("#birthday");
    const status = document.querySelector("#status");

    formId.id = "";
    group.value = "";
    name.value = "";
    surname.value = "";
    gender.value = "";
    birthday.value = "";
    status.checked = false;

    const groupWarning = document.querySelector("#dialog-group-warning");
    const nameWarning = document.querySelector("#dialog-name-warning");
    const surnameWarning = document.querySelector("#dialog-surname-warning");
    const genderWarning = document.querySelector("#dialog-gender-warning");
    const birthdayWarning = document.querySelector("#dialog-birthday-warning");

    groupWarning.style.display = "none";
    nameWarning.style.display = "none";
    surnameWarning.style.display = "none";
    genderWarning.style.display = "none";
    birthdayWarning.style.display = "none";
}



