<?php
include "config.php";

header('Content-Type: application/json');
$response = array(
    'success' => true,
    'message' => "Data was processed successfully"
);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];

        $sql = "DELETE FROM students WHERE id = '$id'";

        if ($conn->query($sql) === TRUE) {
            $response['success'] = true;
        } else {
            $response['success'] = false;
            $response['message'] = "Error deleting student from the database";
        }
    }
    else {
        $response['success'] = false;
        $response['message'] = "Not all of the parameters are set";
    }
}
else {
    $response['success'] = false;
    $response['message'] = "This is not a POST request";
}

echo json_encode($response);
?>