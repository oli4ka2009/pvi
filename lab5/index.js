/*window.onload = async function () {
    if (navigator.serviceWorker) {
        try {
            await navigator.serviceWorker.register("serviceWorker.js");
        } catch (error) {
            console.log("Can't register serviceWorker", error);
        }
    }
};*/

const sideMenu = document.querySelector("aside");
const menuBtn = document.querySelector("#menu-btn");
const closeBtn = document.querySelector("#close-btn");
const addStudentBtn = document.querySelector('#add-student');
const deleteStudentButtons = document.querySelectorAll('.delete-btn');
const editStudentButtons = document.querySelectorAll('.edit-btn');
const cancelDeleteDialogButton = document.querySelector(
    ".delete-student-cancel"
);
const closeDeleteDialogButton = document.querySelector(".delete-student-close");
const submitDialogButton = document.querySelector(".dialog-submit");
const closeAddEditDialogButton = document.querySelector(".form-close");
const cancelButton = document.querySelector(".dialog-cancel");

menuBtn.addEventListener('click', () => {
    sideMenu.style.display = 'block';
});

closeBtn.addEventListener('click', () => {
    sideMenu.style.display = 'none';
});

deleteStudentButtons.forEach((button) => {button.addEventListener('click', () => deleteDialogFunction(button))});
editStudentButtons.forEach((button) => {button.addEventListener('click', () => AddAndEditDialogFunction(button))});
addStudentBtn.addEventListener('click', () => AddAndEditDialogFunction(addStudentBtn));

closeAddEditDialogButton.addEventListener("click", () => {
    let dialog = document.querySelector(".dialogOverlay");
    dialog.close();
})

cancelDeleteDialogButton.addEventListener("click", () => {
    let deleteDialog = document.querySelector(".delete-student-dialog");
    deleteDialog.close();
});

closeDeleteDialogButton.addEventListener("click", () => {
    let deleteDialog = document.querySelector(".delete-student-dialog");
    deleteDialog.close();
});

cancelButton.addEventListener("click", () => {
    let dialog = document.querySelector(".dialogOverlay");
    dialog.close();
})

function deleteDialogFunction(button) {
    let deleteDialog = document.querySelector(".delete-student-dialog");
    deleteDialog.showModal();

    let deleteButton = document.querySelector(".delete-student-confirm");
    deleteButton.addEventListener("click", function () {
        deleteStudents(button);
        deleteDialog.close();
    });
}

function deleteStudents(button) {
    let row = button.closest('tr');

    let data = {
        id: row.getAttribute("data-id")
    }

    if (row) {
        $.ajax({
            url: 'delete.php',
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function(data) {
                if (data.success) {
                    row.remove();
                } else {
                    console.error("An error occurred while processing data on the server: " + data.message);
                }
            },
            error: function(xhr, status, error) {
                console.error('Error sending request to server:', error);
            }
        });
    } else {
        console.log("Рядок не знайдено");
    }
}

function Student() {
    this.id = "";
    this.group = "";
    this.surname = "";
    this.name = "";
    this.gender = "";
    this.birthday = "";
    this.status = false;
}

//Додавання та редагування студента
function AddAndEditDialogFunction(button) {
    const groupWarning = document.querySelector("#dialog-group-warning");
    const nameWarning = document.querySelector("#dialog-name-warning");
    const surnameWarning = document.querySelector("#dialog-surname-warning");
    const genderWarning = document.querySelector("#dialog-gender-warning");
    const birthdayWarning = document.querySelector("#dialog-birthday-warning");

    groupWarning.style.display = "none";
    nameWarning.style.display = "none";
    surnameWarning.style.display = "none";
    genderWarning.style.display = "none";
    birthdayWarning.style.display = "none";

    console.log(button);
    let buttonId = button.getAttribute('data-id');
    let dialogOverlay = document.querySelector(".dialogOverlay");
    let form = document.querySelector("form");
    let formID = form.querySelector("input[type=hidden]");
    formID.id = button.getAttribute('data-id');

    dialogOverlay.showModal();

    let formTitle = document.querySelector(".dialog-heading");

    const group = document.querySelector("#group");
    const name = document.querySelector("#name");
    const surname = document.querySelector("#surname");
    const gender = document.querySelector("#gender");
    const birthday = document.querySelector("#birthday");
    const status = document.querySelector("#status");

    let student = new Student();
    student.id = buttonId;

    if (student.id) {
        console.log("Edit button");
        formTitle.innerHTML = "Edit student";
        let rows = document.querySelectorAll('tr');
        for (i=0; i<rows.length; ++i) {
            if (rows[i].getAttribute('data-id') == student.id) {
                row = rows[i];
                break;
            }
        }
        let nameArray = row.cells[2].innerHTML.split(" ");
        student.name = nameArray[0];
        student.surname = nameArray[1];
        for (let i = 1; i < group.options.length; ++i) {
            if (row.cells[1].innerText == group.querySelector('option[value="' + i + '"]').textContent) {
                student.group = i;
                break;
            }
        }
        for (let i = 1; i < gender.options.length; ++i) {
            if (row.cells[3].innerText == gender.querySelector('option[value="' + i + '"]').textContent) {
                student.gender = i;
                break;
            }
        }
        let birthdayArray = row.cells[4].innerHTML.split(".");
        student.birthday = `${birthdayArray[2]}-${birthdayArray[1]}-${birthdayArray[0]}`;
        let statusRow = row.cells[5].querySelector("span");
        student.status = statusRow.classList.contains("active") ? true : false;
    }
    else {
        console.log("Add button");
        formTitle.innerHTML = "Add student";
    }
    group.value = student.group;
    name.value = student.name;
    surname.value = student.surname;
    gender.value = student.gender;
    birthday.value = student.birthday;
    status.checked = student.status;
    dialogOverlay.showModal();
}

submitDialogButton.addEventListener("click", (event) => {
    event.preventDefault();
        let formID = document.querySelector("input[type=hidden]").id;

        if (formID) {
            EditStudent(formID);
        } else {
            AddStudent();
        }
});

function EditStudent(formID) {
    const form = document.querySelector(".dialogOverlay");

    let row = null;
    let rows = document.querySelectorAll('tr');
    for (i = 0; i < rows.length; ++i) {
        if (rows[i].getAttribute('data-id') == formID) {
            row = rows[i];
        }
    }

    const group = document.querySelector("#group");
    const name = document.querySelector("#name");
    const surname = document.querySelector("#surname");
    const gender = document.querySelector("#gender");
    const birthday = document.querySelector("#birthday");
    const status = document.querySelector("#status");

    let formattedBirthday = "";
    let birthdayArray = birthday.value.split("-");
    formattedBirthday = `${birthdayArray[2]}.${birthdayArray[1]}.${birthdayArray[0]}`;

    let groupValue = group.value;
    let genderValue = gender.value;

    let rowData = {
        id: formID,
        group: groupValue,
        firstName: name.value,
        lastName: surname.value,
        gender: genderValue,
        birthday: formattedBirthday,
        status: !!status.checked ? 1 : 0
    };

    let jsonRow = JSON.stringify(rowData);
    console.log(jsonRow);

    $.ajax({
        url: 'server.php',
        type: 'POST',
        data: rowData,
        dataType: 'json',
        success: function(data) {
            if (data.success) {
                console.log("The data was successfully processed on the server");
                row.cells[1].setAttribute("data-val", groupValue)
                row.cells[1].innerHTML = document.querySelector('#group option[value="' + groupValue + '"]').textContent;
                row.cells[2].innerHTML = `${name.value} ${surname.value}`;
                row.cells[3].innerHTML = document.querySelector('#gender option[value="' + genderValue + '"]').textContent;;
                row.cells[3].setAttribute("data-val", genderValue)
                row.cells[4].innerHTML = formattedBirthday;
                row.cells[5].innerHTML = status.checked ? `<span class="status active"></span>` : `<span class="status nonactive"></span>`;

                form.close();
            } else {
                const groupWarning = $("#dialog-group-warning");
                const nameWarning = $("#dialog-name-warning");
                const surnameWarning = $("#dialog-surname-warning");
                const genderWarning = $("#dialog-gender-warning");
                const birthdayWarning = $("#dialog-birthday-warning");
                if (data.error.code == 1) {
                    groupWarning.css("display", "block");
                } else {
                    groupWarning.css("display", "none");
                }
                if (data.error.code == 2) {
                    nameWarning.css("display", "block");
                } else {
                    nameWarning.css("display", "none");
                }
                if (data.error.code == 3) {
                    surnameWarning.css("display", "block");
                } else {
                    surnameWarning.css("display", "none");
                }
                if (data.error.code == 4) {
                    genderWarning.css("display", "block");
                } else {
                    genderWarning.css("display", "none");
                }
                if (data.error.code == 5) {
                    birthdayWarning.css("display", "block");
                } else {
                    birthdayWarning.css("display", "none");
                }
                console.error("An error occurred while processing data on the server: " + data.error.message);
            }
        },
        error: function(xhr, status, error) {
            console.error('Error sending request to server:', error);
        }
    });
}

function AddStudent() {
    const group = document.querySelector("#group");
    const name = document.querySelector("#name");
    const surname = document.querySelector("#surname");
    const gender = document.querySelector("#gender");
    const birthday = document.querySelector("#birthday");
    const status = document.querySelector("#status");

    let formattedBirthday = "";
    let birthdayArray = birthday.value.split("-");
    formattedBirthday = `${birthdayArray[2]}.${birthdayArray[1]}.${birthdayArray[0]}`;

    const table = document.querySelector("table");
    const tableBody = table.querySelector("tbody");

    let rowData = {
        id: "",
        group: group.value,
        firstName: name.value,
        lastName: surname.value,
        gender: gender.value,
        birthday: formattedBirthday,
        status: !!status.checked ? 1 : 0
    };

    let jsonRow = JSON.stringify(rowData);
    console.log(jsonRow);

    $.ajax({
        url: 'server.php',
        type: 'POST',
        data: rowData,
        dataType: 'json',
        success: function(data) {
            if (data.success) {
                console.log("The data was successfully processed on the server");
                let newRow=tableBody.insertRow();
                newRow.insertCell(0).innerHTML = `<input type="checkbox">`;
                newRow.insertCell(1).innerHTML = group.querySelector('option[value="' + group.value + '"]').textContent;
                newRow.insertCell(2).innerHTML = `${name.value} ${surname.value}`;
                newRow.insertCell(3).innerHTML = gender.querySelector('option[value="' + gender.value + '"]').textContent;
                newRow.insertCell(4).innerHTML = formattedBirthday;
                newRow.insertCell(5).innerHTML = status.checked ? '<span class="status active"></span>' : '<span class="status nonactive"></span>';
                newRow.insertCell(
                    6
                ).innerHTML = `<button class="table-buttons" data-id=${data.id}><span class="material-symbols-outlined edit-btn">edit</span></button><button class="table-buttons delete-btn" data-id=${data.id}><span class="material-symbols-outlined">delete</span></button>`;

                newRow.setAttribute('data-id', data.id);
                console.log(newRow.getAttribute('data-id'));

                tableBody.appendChild(newRow);

                newRow
                    .querySelector(".delete-btn")
                    .addEventListener("click", () =>
                        deleteDialogFunction(newRow.querySelector(".delete-btn"))
                    );
                newRow.querySelector(".delete-btn").setAttribute('data-id', data.id);
                newRow.querySelector(".edit-btn").addEventListener("click", () => AddAndEditDialogFunction(newRow.querySelector(".edit-btn")));
                newRow.querySelector(".edit-btn").setAttribute('data-id', data.id);
                let form = document.querySelector(".dialogOverlay");
                form.close();
            } else {
                const groupWarning = document.querySelector("#dialog-group-warning");
                const nameWarning = document.querySelector("#dialog-name-warning");
                const surnameWarning = document.querySelector("#dialog-surname-warning");
                const genderWarning = document.querySelector("#dialog-gender-warning");
                const birthdayWarning = document.querySelector("#dialog-birthday-warning");
                if (data.error.code == 1) {
                    groupWarning.style.display = "block";
                }
                else {
                    groupWarning.style.display = "none";
                }
                if (data.error.code == 2) {
                    nameWarning.style.display = "block";
                }
                else {
                    nameWarning.style.display = "none";
                }
                if (data.error.code == 3) {
                    surnameWarning.style.display = "block";
                }
                else {
                    surnameWarning.style.display = "none";
                }
                if (data.error.code == 4) {
                    genderWarning.style.display = "block";
                }
                else {
                    genderWarning.style.display = "none";
                }
                if (data.error.code == 5) {
                    birthdayWarning.style.display = "block";
                }
                else {
                    birthdayWarning.style.display = "none";
                }
                console.error("An error occurred while processing data on the server: " + data.error.message);
            }
        },
    error: function(xhr, status, error) {
        console.error('Error sending request to server:', error);
    }
});
}



